//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.8-b130911.1802 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2024.07.02 às 09:09:41 AM BRT 
//


package br.inf.portalfiscal.mdfe;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.inf.portalfiscal.mdfe package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MDFe_QNAME = new QName("http://www.portalfiscal.inf.br/mdfe", "MDFe");
    private final static QName _Signature_QNAME = new QName("http://www.w3.org/2000/09/xmldsig#", "Signature");
    private final static QName _EventoMDFe_QNAME = new QName("http://www.portalfiscal.inf.br/mdfe", "eventoMDFe");
    private final static QName _RetEventoMDFe_QNAME = new QName("http://www.portalfiscal.inf.br/mdfe", "retEventoMDFe");
    private final static QName _RetConsSitMDFe_QNAME = new QName("http://www.portalfiscal.inf.br/mdfe", "retConsSitMDFe");
    private final static QName _RetEnviMDFe_QNAME = new QName("http://www.portalfiscal.inf.br/mdfe", "retEnviMDFe");
    private final static QName _RetConsReciMDFe_QNAME = new QName("http://www.portalfiscal.inf.br/mdfe", "retConsReciMDFe");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.inf.portalfiscal.mdfe
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EvIncCondutorMDFe }
     * 
     */
    public EvIncCondutorMDFe createEvIncCondutorMDFe() {
        return new EvIncCondutorMDFe();
    }

    /**
     * Create an instance of {@link Rodo }
     * 
     */
    public Rodo createRodo() {
        return new Rodo();
    }

    /**
     * Create an instance of {@link ReferenceType }
     * 
     */
    public ReferenceType createReferenceType() {
        return new ReferenceType();
    }

    /**
     * Create an instance of {@link SignedInfoType }
     * 
     */
    public SignedInfoType createSignedInfoType() {
        return new SignedInfoType();
    }

    /**
     * Create an instance of {@link TProtMDFe }
     * 
     */
    public TProtMDFe createTProtMDFe() {
        return new TProtMDFe();
    }

    /**
     * Create an instance of {@link TUnidadeTransp }
     * 
     */
    public TUnidadeTransp createTUnidadeTransp() {
        return new TUnidadeTransp();
    }

    /**
     * Create an instance of {@link TUnidCarga }
     * 
     */
    public TUnidCarga createTUnidCarga() {
        return new TUnidCarga();
    }

    /**
     * Create an instance of {@link TNFeNF }
     * 
     */
    public TNFeNF createTNFeNF() {
        return new TNFeNF();
    }

    /**
     * Create an instance of {@link TNFeNF.InfNF }
     * 
     */
    public TNFeNF.InfNF createTNFeNFInfNF() {
        return new TNFeNF.InfNF();
    }

    /**
     * Create an instance of {@link Rodo.VeicReboque }
     * 
     */
    public Rodo.VeicReboque createRodoVeicReboque() {
        return new Rodo.VeicReboque();
    }

    /**
     * Create an instance of {@link Rodo.VeicTracao }
     * 
     */
    public Rodo.VeicTracao createRodoVeicTracao() {
        return new Rodo.VeicTracao();
    }

    /**
     * Create an instance of {@link Rodo.InfANTT }
     * 
     */
    public Rodo.InfANTT createRodoInfANTT() {
        return new Rodo.InfANTT();
    }

    /**
     * Create an instance of {@link Rodo.InfANTT.InfPag }
     * 
     */
    public Rodo.InfANTT.InfPag createRodoInfANTTInfPag() {
        return new Rodo.InfANTT.InfPag();
    }

    /**
     * Create an instance of {@link Rodo.InfANTT.InfContratante }
     * 
     */
    public Rodo.InfANTT.InfContratante createRodoInfANTTInfContratante() {
        return new Rodo.InfANTT.InfContratante();
    }

    /**
     * Create an instance of {@link Rodo.InfANTT.ValePed }
     * 
     */
    public Rodo.InfANTT.ValePed createRodoInfANTTValePed() {
        return new Rodo.InfANTT.ValePed();
    }

    /**
     * Create an instance of {@link TRetConsSitMDFe }
     * 
     */
    public TRetConsSitMDFe createTRetConsSitMDFe() {
        return new TRetConsSitMDFe();
    }

    /**
     * Create an instance of {@link TRetEnviMDFe }
     * 
     */
    public TRetEnviMDFe createTRetEnviMDFe() {
        return new TRetEnviMDFe();
    }

    /**
     * Create an instance of {@link TMDFe }
     * 
     */
    public TMDFe createTMDFe() {
        return new TMDFe();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe }
     * 
     */
    public TMDFe.InfMDFe createTMDFeInfMDFe() {
        return new TMDFe.InfMDFe();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfPAA }
     * 
     */
    public TMDFe.InfMDFe.InfPAA createTMDFeInfMDFeInfPAA() {
        return new TMDFe.InfMDFe.InfPAA();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.ProdPred }
     * 
     */
    public TMDFe.InfMDFe.ProdPred createTMDFeInfMDFeProdPred() {
        return new TMDFe.InfMDFe.ProdPred();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.ProdPred.InfLotacao }
     * 
     */
    public TMDFe.InfMDFe.ProdPred.InfLotacao createTMDFeInfMDFeProdPredInfLotacao() {
        return new TMDFe.InfMDFe.ProdPred.InfLotacao();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.Seg }
     * 
     */
    public TMDFe.InfMDFe.Seg createTMDFeInfMDFeSeg() {
        return new TMDFe.InfMDFe.Seg();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfDoc }
     * 
     */
    public TMDFe.InfMDFe.InfDoc createTMDFeInfMDFeInfDoc() {
        return new TMDFe.InfMDFe.InfDoc();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfDoc.InfMunDescarga }
     * 
     */
    public TMDFe.InfMDFe.InfDoc.InfMunDescarga createTMDFeInfMDFeInfDocInfMunDescarga() {
        return new TMDFe.InfMDFe.InfDoc.InfMunDescarga();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfMDFeTransp }
     * 
     */
    public TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfMDFeTransp createTMDFeInfMDFeInfDocInfMunDescargaInfMDFeTransp() {
        return new TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfMDFeTransp();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfNFe }
     * 
     */
    public TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfNFe createTMDFeInfMDFeInfDocInfMunDescargaInfNFe() {
        return new TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfNFe();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe }
     * 
     */
    public TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe createTMDFeInfMDFeInfDocInfMunDescargaInfCTe() {
        return new TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.Ide }
     * 
     */
    public TMDFe.InfMDFe.Ide createTMDFeInfMDFeIde() {
        return new TMDFe.InfMDFe.Ide();
    }

    /**
     * Create an instance of {@link TRetEvento }
     * 
     */
    public TRetEvento createTRetEvento() {
        return new TRetEvento();
    }

    /**
     * Create an instance of {@link TEvento }
     * 
     */
    public TEvento createTEvento() {
        return new TEvento();
    }

    /**
     * Create an instance of {@link TEvento.InfEvento }
     * 
     */
    public TEvento.InfEvento createTEventoInfEvento() {
        return new TEvento.InfEvento();
    }

    /**
     * Create an instance of {@link TEvento.InfEvento.InfPAA }
     * 
     */
    public TEvento.InfEvento.InfPAA createTEventoInfEventoInfPAA() {
        return new TEvento.InfEvento.InfPAA();
    }

    /**
     * Create an instance of {@link EvCancMDFe }
     * 
     */
    public EvCancMDFe createEvCancMDFe() {
        return new EvCancMDFe();
    }

    /**
     * Create an instance of {@link TRetConsReciMDFe }
     * 
     */
    public TRetConsReciMDFe createTRetConsReciMDFe() {
        return new TRetConsReciMDFe();
    }

    /**
     * Create an instance of {@link EvEncMDFe }
     * 
     */
    public EvEncMDFe createEvEncMDFe() {
        return new EvEncMDFe();
    }

    /**
     * Create an instance of {@link EvIncCondutorMDFe.Condutor }
     * 
     */
    public EvIncCondutorMDFe.Condutor createEvIncCondutorMDFeCondutor() {
        return new EvIncCondutorMDFe.Condutor();
    }

    /**
     * Create an instance of {@link Rodo.LacRodo }
     * 
     */
    public Rodo.LacRodo createRodoLacRodo() {
        return new Rodo.LacRodo();
    }

    /**
     * Create an instance of {@link TRSAKeyValueType }
     * 
     */
    public TRSAKeyValueType createTRSAKeyValueType() {
        return new TRSAKeyValueType();
    }

    /**
     * Create an instance of {@link TConsReciMDFe }
     * 
     */
    public TConsReciMDFe createTConsReciMDFe() {
        return new TConsReciMDFe();
    }

    /**
     * Create an instance of {@link TEnviMDFe }
     * 
     */
    public TEnviMDFe createTEnviMDFe() {
        return new TEnviMDFe();
    }

    /**
     * Create an instance of {@link TRespTec }
     * 
     */
    public TRespTec createTRespTec() {
        return new TRespTec();
    }

    /**
     * Create an instance of {@link TEndernac }
     * 
     */
    public TEndernac createTEndernac() {
        return new TEndernac();
    }

    /**
     * Create an instance of {@link TEndereco }
     * 
     */
    public TEndereco createTEndereco() {
        return new TEndereco();
    }

    /**
     * Create an instance of {@link TConsSitMDFe }
     * 
     */
    public TConsSitMDFe createTConsSitMDFe() {
        return new TConsSitMDFe();
    }

    /**
     * Create an instance of {@link TLocal }
     * 
     */
    public TLocal createTLocal() {
        return new TLocal();
    }

    /**
     * Create an instance of {@link TEndeEmi }
     * 
     */
    public TEndeEmi createTEndeEmi() {
        return new TEndeEmi();
    }

    /**
     * Create an instance of {@link TRetMDFe }
     * 
     */
    public TRetMDFe createTRetMDFe() {
        return new TRetMDFe();
    }

    /**
     * Create an instance of {@link TEnderFer }
     * 
     */
    public TEnderFer createTEnderFer() {
        return new TEnderFer();
    }

    /**
     * Create an instance of {@link TProcEvento }
     * 
     */
    public TProcEvento createTProcEvento() {
        return new TProcEvento();
    }

    /**
     * Create an instance of {@link TEndOrg }
     * 
     */
    public TEndOrg createTEndOrg() {
        return new TEndOrg();
    }

    /**
     * Create an instance of {@link TEndReEnt }
     * 
     */
    public TEndReEnt createTEndReEnt() {
        return new TEndReEnt();
    }

    /**
     * Create an instance of {@link SignatureType }
     * 
     */
    public SignatureType createSignatureType() {
        return new SignatureType();
    }

    /**
     * Create an instance of {@link X509DataType }
     * 
     */
    public X509DataType createX509DataType() {
        return new X509DataType();
    }

    /**
     * Create an instance of {@link SignatureValueType }
     * 
     */
    public SignatureValueType createSignatureValueType() {
        return new SignatureValueType();
    }

    /**
     * Create an instance of {@link TransformsType }
     * 
     */
    public TransformsType createTransformsType() {
        return new TransformsType();
    }

    /**
     * Create an instance of {@link TransformType }
     * 
     */
    public TransformType createTransformType() {
        return new TransformType();
    }

    /**
     * Create an instance of {@link KeyInfoType }
     * 
     */
    public KeyInfoType createKeyInfoType() {
        return new KeyInfoType();
    }

    /**
     * Create an instance of {@link ReferenceType.DigestMethod }
     * 
     */
    public ReferenceType.DigestMethod createReferenceTypeDigestMethod() {
        return new ReferenceType.DigestMethod();
    }

    /**
     * Create an instance of {@link SignedInfoType.CanonicalizationMethod }
     * 
     */
    public SignedInfoType.CanonicalizationMethod createSignedInfoTypeCanonicalizationMethod() {
        return new SignedInfoType.CanonicalizationMethod();
    }

    /**
     * Create an instance of {@link SignedInfoType.SignatureMethod }
     * 
     */
    public SignedInfoType.SignatureMethod createSignedInfoTypeSignatureMethod() {
        return new SignedInfoType.SignatureMethod();
    }

    /**
     * Create an instance of {@link TProtMDFe.InfProt }
     * 
     */
    public TProtMDFe.InfProt createTProtMDFeInfProt() {
        return new TProtMDFe.InfProt();
    }

    /**
     * Create an instance of {@link TProtMDFe.InfFisco }
     * 
     */
    public TProtMDFe.InfFisco createTProtMDFeInfFisco() {
        return new TProtMDFe.InfFisco();
    }

    /**
     * Create an instance of {@link TUnidadeTransp.LacUnidTransp }
     * 
     */
    public TUnidadeTransp.LacUnidTransp createTUnidadeTranspLacUnidTransp() {
        return new TUnidadeTransp.LacUnidTransp();
    }

    /**
     * Create an instance of {@link TUnidCarga.LacUnidCarga }
     * 
     */
    public TUnidCarga.LacUnidCarga createTUnidCargaLacUnidCarga() {
        return new TUnidCarga.LacUnidCarga();
    }

    /**
     * Create an instance of {@link TNFeNF.InfNFe }
     * 
     */
    public TNFeNF.InfNFe createTNFeNFInfNFe() {
        return new TNFeNF.InfNFe();
    }

    /**
     * Create an instance of {@link TNFeNF.InfNF.Emi }
     * 
     */
    public TNFeNF.InfNF.Emi createTNFeNFInfNFEmi() {
        return new TNFeNF.InfNF.Emi();
    }

    /**
     * Create an instance of {@link TNFeNF.InfNF.Dest }
     * 
     */
    public TNFeNF.InfNF.Dest createTNFeNFInfNFDest() {
        return new TNFeNF.InfNF.Dest();
    }

    /**
     * Create an instance of {@link Rodo.VeicReboque.Prop }
     * 
     */
    public Rodo.VeicReboque.Prop createRodoVeicReboqueProp() {
        return new Rodo.VeicReboque.Prop();
    }

    /**
     * Create an instance of {@link Rodo.VeicTracao.Prop }
     * 
     */
    public Rodo.VeicTracao.Prop createRodoVeicTracaoProp() {
        return new Rodo.VeicTracao.Prop();
    }

    /**
     * Create an instance of {@link Rodo.VeicTracao.Condutor }
     * 
     */
    public Rodo.VeicTracao.Condutor createRodoVeicTracaoCondutor() {
        return new Rodo.VeicTracao.Condutor();
    }

    /**
     * Create an instance of {@link Rodo.InfANTT.InfCIOT }
     * 
     */
    public Rodo.InfANTT.InfCIOT createRodoInfANTTInfCIOT() {
        return new Rodo.InfANTT.InfCIOT();
    }

    /**
     * Create an instance of {@link Rodo.InfANTT.InfPag.Comp }
     * 
     */
    public Rodo.InfANTT.InfPag.Comp createRodoInfANTTInfPagComp() {
        return new Rodo.InfANTT.InfPag.Comp();
    }

    /**
     * Create an instance of {@link Rodo.InfANTT.InfPag.InfPrazo }
     * 
     */
    public Rodo.InfANTT.InfPag.InfPrazo createRodoInfANTTInfPagInfPrazo() {
        return new Rodo.InfANTT.InfPag.InfPrazo();
    }

    /**
     * Create an instance of {@link Rodo.InfANTT.InfPag.InfBanc }
     * 
     */
    public Rodo.InfANTT.InfPag.InfBanc createRodoInfANTTInfPagInfBanc() {
        return new Rodo.InfANTT.InfPag.InfBanc();
    }

    /**
     * Create an instance of {@link Rodo.InfANTT.InfContratante.InfContrato }
     * 
     */
    public Rodo.InfANTT.InfContratante.InfContrato createRodoInfANTTInfContratanteInfContrato() {
        return new Rodo.InfANTT.InfContratante.InfContrato();
    }

    /**
     * Create an instance of {@link Rodo.InfANTT.ValePed.Disp }
     * 
     */
    public Rodo.InfANTT.ValePed.Disp createRodoInfANTTValePedDisp() {
        return new Rodo.InfANTT.ValePed.Disp();
    }

    /**
     * Create an instance of {@link TRetConsSitMDFe.ProtMDFe }
     * 
     */
    public TRetConsSitMDFe.ProtMDFe createTRetConsSitMDFeProtMDFe() {
        return new TRetConsSitMDFe.ProtMDFe();
    }

    /**
     * Create an instance of {@link TRetConsSitMDFe.ProcEventoMDFe }
     * 
     */
    public TRetConsSitMDFe.ProcEventoMDFe createTRetConsSitMDFeProcEventoMDFe() {
        return new TRetConsSitMDFe.ProcEventoMDFe();
    }

    /**
     * Create an instance of {@link TRetConsSitMDFe.ProcInfraSA }
     * 
     */
    public TRetConsSitMDFe.ProcInfraSA createTRetConsSitMDFeProcInfraSA() {
        return new TRetConsSitMDFe.ProcInfraSA();
    }

    /**
     * Create an instance of {@link TRetEnviMDFe.InfRec }
     * 
     */
    public TRetEnviMDFe.InfRec createTRetEnviMDFeInfRec() {
        return new TRetEnviMDFe.InfRec();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFeSupl }
     * 
     */
    public TMDFe.InfMDFeSupl createTMDFeInfMDFeSupl() {
        return new TMDFe.InfMDFeSupl();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.Emit }
     * 
     */
    public TMDFe.InfMDFe.Emit createTMDFeInfMDFeEmit() {
        return new TMDFe.InfMDFe.Emit();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfModal }
     * 
     */
    public TMDFe.InfMDFe.InfModal createTMDFeInfMDFeInfModal() {
        return new TMDFe.InfMDFe.InfModal();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.Tot }
     * 
     */
    public TMDFe.InfMDFe.Tot createTMDFeInfMDFeTot() {
        return new TMDFe.InfMDFe.Tot();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.Lacres }
     * 
     */
    public TMDFe.InfMDFe.Lacres createTMDFeInfMDFeLacres() {
        return new TMDFe.InfMDFe.Lacres();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.AutXML }
     * 
     */
    public TMDFe.InfMDFe.AutXML createTMDFeInfMDFeAutXML() {
        return new TMDFe.InfMDFe.AutXML();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfAdic }
     * 
     */
    public TMDFe.InfMDFe.InfAdic createTMDFeInfMDFeInfAdic() {
        return new TMDFe.InfMDFe.InfAdic();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfSolicNFF }
     * 
     */
    public TMDFe.InfMDFe.InfSolicNFF createTMDFeInfMDFeInfSolicNFF() {
        return new TMDFe.InfMDFe.InfSolicNFF();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfPAA.PAASignature }
     * 
     */
    public TMDFe.InfMDFe.InfPAA.PAASignature createTMDFeInfMDFeInfPAAPAASignature() {
        return new TMDFe.InfMDFe.InfPAA.PAASignature();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.ProdPred.InfLotacao.InfLocalCarrega }
     * 
     */
    public TMDFe.InfMDFe.ProdPred.InfLotacao.InfLocalCarrega createTMDFeInfMDFeProdPredInfLotacaoInfLocalCarrega() {
        return new TMDFe.InfMDFe.ProdPred.InfLotacao.InfLocalCarrega();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.ProdPred.InfLotacao.InfLocalDescarrega }
     * 
     */
    public TMDFe.InfMDFe.ProdPred.InfLotacao.InfLocalDescarrega createTMDFeInfMDFeProdPredInfLotacaoInfLocalDescarrega() {
        return new TMDFe.InfMDFe.ProdPred.InfLotacao.InfLocalDescarrega();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.Seg.InfResp }
     * 
     */
    public TMDFe.InfMDFe.Seg.InfResp createTMDFeInfMDFeSegInfResp() {
        return new TMDFe.InfMDFe.Seg.InfResp();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.Seg.InfSeg }
     * 
     */
    public TMDFe.InfMDFe.Seg.InfSeg createTMDFeInfMDFeSegInfSeg() {
        return new TMDFe.InfMDFe.Seg.InfSeg();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfMDFeTransp.Peri }
     * 
     */
    public TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfMDFeTransp.Peri createTMDFeInfMDFeInfDocInfMunDescargaInfMDFeTranspPeri() {
        return new TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfMDFeTransp.Peri();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfNFe.Peri }
     * 
     */
    public TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfNFe.Peri createTMDFeInfMDFeInfDocInfMunDescargaInfNFePeri() {
        return new TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfNFe.Peri();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe.Peri }
     * 
     */
    public TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe.Peri createTMDFeInfMDFeInfDocInfMunDescargaInfCTePeri() {
        return new TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe.Peri();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe.InfEntregaParcial }
     * 
     */
    public TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe.InfEntregaParcial createTMDFeInfMDFeInfDocInfMunDescargaInfCTeInfEntregaParcial() {
        return new TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe.InfEntregaParcial();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe.InfNFePrestParcial }
     * 
     */
    public TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe.InfNFePrestParcial createTMDFeInfMDFeInfDocInfMunDescargaInfCTeInfNFePrestParcial() {
        return new TMDFe.InfMDFe.InfDoc.InfMunDescarga.InfCTe.InfNFePrestParcial();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.Ide.InfMunCarrega }
     * 
     */
    public TMDFe.InfMDFe.Ide.InfMunCarrega createTMDFeInfMDFeIdeInfMunCarrega() {
        return new TMDFe.InfMDFe.Ide.InfMunCarrega();
    }

    /**
     * Create an instance of {@link TMDFe.InfMDFe.Ide.InfPercurso }
     * 
     */
    public TMDFe.InfMDFe.Ide.InfPercurso createTMDFeInfMDFeIdeInfPercurso() {
        return new TMDFe.InfMDFe.Ide.InfPercurso();
    }

    /**
     * Create an instance of {@link TRetEvento.InfEvento }
     * 
     */
    public TRetEvento.InfEvento createTRetEventoInfEvento() {
        return new TRetEvento.InfEvento();
    }

    /**
     * Create an instance of {@link TEvento.InfEvento.DetEvento }
     * 
     */
    public TEvento.InfEvento.DetEvento createTEventoInfEventoDetEvento() {
        return new TEvento.InfEvento.DetEvento();
    }

    /**
     * Create an instance of {@link TEvento.InfEvento.InfSolicNFF }
     * 
     */
    public TEvento.InfEvento.InfSolicNFF createTEventoInfEventoInfSolicNFF() {
        return new TEvento.InfEvento.InfSolicNFF();
    }

    /**
     * Create an instance of {@link TEvento.InfEvento.InfPAA.PAASignature }
     * 
     */
    public TEvento.InfEvento.InfPAA.PAASignature createTEventoInfEventoInfPAAPAASignature() {
        return new TEvento.InfEvento.InfPAA.PAASignature();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TMDFe }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/mdfe", name = "MDFe")
    public JAXBElement<TMDFe> createMDFe(TMDFe value) {
        return new JAXBElement<TMDFe>(_MDFe_QNAME, TMDFe.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignatureType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.w3.org/2000/09/xmldsig#", name = "Signature")
    public JAXBElement<SignatureType> createSignature(SignatureType value) {
        return new JAXBElement<SignatureType>(_Signature_QNAME, SignatureType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TEvento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/mdfe", name = "eventoMDFe")
    public JAXBElement<TEvento> createEventoMDFe(TEvento value) {
        return new JAXBElement<TEvento>(_EventoMDFe_QNAME, TEvento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRetEvento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/mdfe", name = "retEventoMDFe")
    public JAXBElement<TRetEvento> createRetEventoMDFe(TRetEvento value) {
        return new JAXBElement<TRetEvento>(_RetEventoMDFe_QNAME, TRetEvento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRetConsSitMDFe }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/mdfe", name = "retConsSitMDFe")
    public JAXBElement<TRetConsSitMDFe> createRetConsSitMDFe(TRetConsSitMDFe value) {
        return new JAXBElement<TRetConsSitMDFe>(_RetConsSitMDFe_QNAME, TRetConsSitMDFe.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRetEnviMDFe }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/mdfe", name = "retEnviMDFe")
    public JAXBElement<TRetEnviMDFe> createRetEnviMDFe(TRetEnviMDFe value) {
        return new JAXBElement<TRetEnviMDFe>(_RetEnviMDFe_QNAME, TRetEnviMDFe.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRetConsReciMDFe }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.portalfiscal.inf.br/mdfe", name = "retConsReciMDFe")
    public JAXBElement<TRetConsReciMDFe> createRetConsReciMDFe(TRetConsReciMDFe value) {
        return new JAXBElement<TRetConsReciMDFe>(_RetConsReciMDFe_QNAME, TRetConsReciMDFe.class, null, value);
    }

}
