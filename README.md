# MDF-e

Projeto para geração e compilação das Classes Java
referentes ao projeto MDF-e (Manifesto Eletrônico de Documentos Fiscais).

Este projeto utiliza Maven2.

## XSD - Passo a passo

Baixar arquivos de <https://mdfe-portal.sefaz.rs.gov.br/Site/Documentos>.

Substituir arquivos baixados no diretório `schemas`.

Num terminal executar:

```bash
xjc -nv -d src/main/java/ \
    schemas/mdfe_v3.00.xsd \
    schemas/mdfeModalRodoviario_v3.00.xsd \
    schemas/retEnviMDFe_v3.00.xsd \
    schemas/retConsSitMDFe_v3.00.xsd \
    schemas/retConsReciMDFe_v3.00.xsd \
    schemas/retEventoMDFe_v3.00.xsd \
    schemas/evEncMDFe_v3.00.xsd \
    schemas/evCancMDFe_v3.00.xsd \
    schemas/eventoMDFe_v3.00.xsd \
    schemas/evIncCondutorMDFe_v3.00.xsd \
    -p br.inf.portalfiscal.mdfe
```

**OBS.**: Pode ser que seja necessário modificar a versão!

**OBS.**: Parâmetro `-nv` desabilita **strict validation**. Sem o parâmetro o
erro a seguir estava ocorrendo:

```
[ERROR] A configuração atual do parser não permite que o valor de um atributo maxOccurs seja definido como maior que o valor 5.000.
  linha 336 de file:/home/atila/NetBeansProjects/mdfe/schemas/mdfeTiposBasico_v3.00.xsd

Falha ao fazer parse de um esquema.
```

## WSDL

### Links

* <https://mdfe-portal.sefaz.rs.gov.br/Site/Servicos> - Lista web services

## Passo a passo

`~/dev-lib/java/axis2-1.5.6/bin/wsdl2java.sh -S src/main/java/ -uri wsdl/MDFeRecepcao.wsdl`
