#!/usr/bin/env bash

set -o nounset
set -o errexit
set -o pipefail

xjc -nv -d src/main/java/ \
    schemas/mdfe_v3.00.xsd \
    schemas/mdfeModalRodoviario_v3.00.xsd \
    schemas/evEncMDFe_v3.00.xsd \
    schemas/evCancMDFe_v3.00.xsd \
    schemas/eventoMDFe_v3.00.xsd \
    schemas/evIncCondutorMDFe_v3.00.xsd \
    -p br.inf.portalfiscal.mdfe
